# SymReader

## What is SymReader

SymReader is a library providing a generic way to map a file format in memory
according to its specification. (endian, in memory representation, sections,
rights, etc).

## Context

SymReader is developed as part of the Barghest project (barghest.org).

## Features in development/incomplete
* [in progress] Elf 32/64 executable support
* [planned]     PE, core, object files
